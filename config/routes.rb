Rails.application.routes.draw do

  devise_for :admins, controllers: { sessions: "sessions" }
  devise_for :partners, controllers: { sessions: "sessions", registrations: "registrations" }

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

	authenticated :partner do
	  root to: "establishments#index", as: "partner_root"
	end

  root to: "landing#index"

  resources :dashboard
  resources :establishments

  resources :landing do
    collection do
      get :filter
    end
  end


  namespace :api do
    namespace :v1 do
      resources :directories do
        collection do
          get :cities
          get :categories
          get :establishments
        end
      end
    end
  end


end
