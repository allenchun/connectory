require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module TeamBuilding
  class Application < Rails::Application
    config.active_record.raise_in_transactional_callbacks = true
    config.assets.paths << Rails.root.join('vendor', 'assets', 'components')

    config.assets.precompile += %w( landing.css
                                    landing.js
                                    authentication.css
                                    authentication.js
                                    start.css)

    config.autoload_paths += ["lib/"]
    config.autoload_paths += Dir["#{config.root}/lib/**/",
                                 "#{config.root}/app/presenters/"]

    config.filepicker_rails.api_key = "Your filepicker.io API Key"

    config.generators do |g|
      g.helper false
      g.test_unit false
      g.stylesheets false
      g.javascripts false
      g.controller_specs false
      g.view_specs false
      g.routing_specs false
      g.request_specs false
      g.helper_specs false
      g.jbuilder false
    end

  end
end
