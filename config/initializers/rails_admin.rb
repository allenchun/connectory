RailsAdmin.config do |config|

  config.authenticate_with do
    authenticate_admin!
  end
  
  config.current_user_method(&:current_admin)

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
  end

end
