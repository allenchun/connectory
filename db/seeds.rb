admins = [
  { email: "allen@proudcloud.net", password: "1234qwer", first_name: "Allen", last_name: "Chun" },
  { email: "martin@proudcloud.net", password: "1234qwer", first_name: "Martin", last_name: "Verdejo" },
  { email: "carl@proudcloud.net", password: "1234qwer", first_name: "Carl", last_name: "Flor" },
]
Admin.create(admins)

partners = [
  { email: "partner@proudcloud.net", password: "1234qwer", first_name: "Philippine", last_name: "Government", status: 'active' },
]
Partner.create(partners)
partner = Partner.first

cities = [
  {name: "Caloocan"},
  {name: "Las Pinas"},
  {name: "Makati"},
  {name: "Malabon"},
  {name: "Mandaluyong"},
  {name: "Marikina"},
  {name: "Muntinlupa"},
  {name: "Navotas"},
  {name: "Paranaque"},
  {name: "Pasay"},
  {name: "Pasig"},
  {name: "Quezon City"},
  {name: "San Juan"},
  {name: "Taguig"},
  {name: "Valenzuela"}
]
City.create(cities)

qc = City.find_by(name: "Quezon City")
pasig = City.find_by(name: "Pasig")


categories = [
  { name: "Hospitals" },
  { name: "Police" },
  { name: "Restaurants" },
  { name: "Hotel" },
  { name: "Banks" },
  { name: "Gasoline" },
  { name: "Services" },
  { name: "Government" }
]
Category.create(categories)

bank = Category.find_by(name: "Banks")
service = Category.find_by(name: "Services")

establishments = [
  { name: 'Robinsons Bank', address: 'IBM Plaza, 8 Eastwood Avenue, Quezon City, NCR, Philippines', latitude: 14.607741, longitude: 121.079784, category_id: bank.id , city_id: qc.id, open_hours: '8:00 AM to 3:30 PM Weekdays', landline: '(02) 543 - 9476', mobile: '0993 435 7566', partner_id: partner.id, is_verified: true },
  { name: 'CitiBank', address: 'Citibank Square, Quezon City, NCR, Philippines', latitude: 14.607277, longitude: 121.078803, category_id: bank.id, city_id: qc.id, open_hours: '9:00 AM to 4:30 PM Weekdays', landline: '(02) 324 - 3245', mobile: '0993 217 0372', partner_id: partner.id, is_verified: true },
  { name: 'BPI Family Bank', address: 'Lafeyette 1, Eastwood Avenue, Quezon City, NCR, Philippines', latitude: 14.608642, longitude: 121.079818, category_id: bank.id, city_id: qc.id, open_hours: '8:30 AM to 4:00 PM Weekdays', landline: '(02) 234 - 6485', mobile: '0993 457 9852', partner_id: partner.id, is_verified: true },
  { name: 'Unionbank of the Philippines', address: '184-B Eulogio Rodriguez Jr. Ave Quezon City Metro Manila Philippines', latitude: 14.610345, longitude: 121.078863, category_id: bank.id, city_id: qc.id, open_hours: '8:00 AM to 5:00 PM Weekdays', landline: '(02) 123 - 3456', mobile: '0993 688 9999', partner_id: partner.id, is_verified: true },
  { name: 'Eastwest Bank - C5', address: '184-B Eulogio Rodriguez Jr. Avenue, Quezon City, NCR, Philippines', latitude: 14.609409, longitude: 121.079198, category_id: bank.id, city_id: qc.id, open_hours: '9:00 AM to 3:30 PM Weekdays', landline: '(02) 234 - 5465', mobile: '0993 444 6666', partner_id: partner.id, is_verified: true },
  { name: 'Security Bank', address: 'Security Bank Eastwood Ave Quezon City Metro Manila Philippines', latitude: 14.609408, longitude: 121.079166, category_id: bank.id, city_id: qc.id, open_hours: '8:30 AM to 4:30 PM Weekdays', landline: '(02) 534 - 3688', mobile: '0993 565 3435', partner_id: partner.id, is_verified: true },
  { name: 'Bank of Commerce', address: 'Bank of Commerce C5 Road, Bagumbayan , Bagumbayan , 1109 Quezon City , Metro Manila Philippines', latitude: 14.606659, longitude: 121.079666, category_id: bank.id, city_id: qc.id, open_hours: '8:30 AM to 3:30 PM Weekdays', landline: '(02) 324 - 3548', mobile: '0993 345 4577', partner_id: partner.id, is_verified: true },
  { name: 'Asia United Bank', address: 'AUB Unit D, Aspire Tower 150E E. Rodriguez Jr. Cr. Calle Industria Bagumbayan, Quezon City 1110 Metro Manila, Philippines', latitude: 14.605177, longitude: 121.080790, category_id: bank.id, city_id: qc.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },

  { name: 'Asian Development Bank', address: '6 ADB Ave Ortigas Center, Mandaluyong 1550 Metro Manila Philippines', latitude: 14.588065, longitude: 121.058610, category_id: bank.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },
  { name: 'UnionBank', address: 'UnionBank of the Philippines Centerpoint Condominium, Doña Julia Vargas Ave. cor. Garnet Rd Ortigas Center, Pasig Metro Manila, Philippines', latitude: 14.584442, longitude: 121.062575, category_id: bank.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },
  { name: 'BDO', address: 'BDO Strata 100 - Ortigas Emerald Ave Pasig Metro Manila Philippines', latitude: 14.586598, longitude: 121.061473, category_id: bank.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },
  { name: 'Philippine National Bank', address: 'Philippine National Bank - Shangri-La Plaza Unit AX 116 P3 Carpark Bldg.Shangri-La Annex Plaza Mall, Epifanio de los Santos Ave Mandaluyong Philippines', latitude: 14.581435, longitude: 121.054435, category_id: bank.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },
  { name: 'BPI', address: 'Bank Of The Philippine Islands - Bpi Exchange Road Ortigas Center, Pasig Philippines', latitude: 14.582887, longitude: 121.061831, category_id: bank.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },

  { name: 'Laundry Express', address: 'Laundry Express Edsa Corner Ortigas Avenue, Quezon City Philippines', latitude: 14.591836, longitude: 121.058679, category_id: service.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },
  { name: 'Panda Cleaners', address: 'Panda Cleaner Laundry Shop Ground Floor Unit II Grand Empire Tower, Garnet Road, corner ADB Avenue, Ortigas Center Pasig 1605 Metro Manila Philippines', latitude:14.589415, longitude: 121.061030, category_id: service.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },
  { name: 'Mr. Cleaners', address: 'Mr. Cleaners Plus 103 Sapphire Cor. Onyx Roads, Pasig Philippines', latitude: 14.586548, longitude: 121.062577, category_id: service.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true},
  { name: 'Washlist Laundry', address: 'Washlist Laundry Incorporated 5th Floor, Emerald Mansion, Emerald Avenue Pasig 1600 Metro Manila Philippines', latitude: 14.586467, longitude: 121.061964, category_id: service.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true },
  { name: 'Lets Talk Dirte', address: 'Lets Talk Dirty Escriva Dr Pasig Metro Manila Philippines', latitude: 14.578103, longitude: 121.060051, category_id: service.id, city_id: pasig.id, open_hours: '8:00 AM to 4:30 PM Weekdays', landline: '(02) 575 - 633', mobile: '0993 565 5677', partner_id: partner.id, is_verified: true }
]

Establishment.create(establishments)
