class AddEstablishment < ActiveRecord::Migration
  def change
    create_table(:establishments) do |t|
      t.string :name
      t.string :address
      t.string :city
      t.decimal :longitude
      t.decimal :latitude
      t.string :landline
      t.string :mobile
      t.string :open_hours
    end
  end
end
