class AddCategoryCityToEstablishments < ActiveRecord::Migration
  def change
    add_column :establishments, :category_id, :integer
    add_column :establishments, :city_id, :integer
    add_index :establishments, :city_id
    add_index :establishments, :category_id
  end
end
