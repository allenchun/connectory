class AddPartnerIdToEstablishment < ActiveRecord::Migration
  def change
    add_reference :establishments, :partner, foreign_key: true
    add_index :establishments, :partner_id
  end
end
