class AddVerifiedInEstablishments < ActiveRecord::Migration
  def change
    add_column :establishments, :is_verified, :boolean, default: false
  end
end
