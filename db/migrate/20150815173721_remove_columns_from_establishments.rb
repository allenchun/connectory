class RemoveColumnsFromEstablishments < ActiveRecord::Migration
  def change
    remove_column :establishments, :category
    remove_column :establishments, :city
  end
end
