# Connectory #
Everyone within reach.

A Web and Mobile directory app that works offline.

You can check web app here:
- connectory.herokuapp.com

### Requirements ###

* Rails 4.2.0
* ruby 2.1.4p265
* Less
* Rspec3
* Node.js
* Bower

