class OptionsPresenter

  attr_accessor :cities, :categories

  def intialize
    set_categories
    set_cities
  end

  def initialize
    set_categories
    set_cities
  end



  private

    def set_categories
      @categories = Category.order(:name).map { |category| [category.name, category.id] }
    end

    def set_cities
      @cities = City.order(:name).map { |city| [city.name, city.id] }
    end

end
