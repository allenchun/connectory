# require 'geokit'

class Establishment < ActiveRecord::Base

  # acts_as_mappable
  # geocoded_by :address, units: :km

  validates :city, :address, presence: true

  # after_validation :geocode, if: ->(obj){ obj.address.present? and obj.address_changed? }

  belongs_to :city
  belongs_to :category

  # Scopes
  scope :by_city, proc { |city_id| where(city_id: city_id) unless city_id.blank? }
  scope :by_category, proc { |category_id| where(category_id: category_id) unless category_id.blank? }

  default_scope { where(is_verified: true) }



  def self.search(queries)
    finder = by_city(queries[:city_id])
    finder = finder.by_category(queries[:category_id])
    finder.order(:name)
  end

  def self.coordinates
    order(:name).map { |establishment| [establishment.name.gsub("'", "\\\\'"),
                                        establishment.latitude.to_f,
                                        establishment.longitude.to_f,
                                        establishment.address,
                                        establishment.contact_details,
                                        establishment.id,
                                        establishment.pin_img] }
  end

  def location
    [address, city.name].join(" ")
  end

  def image_url
    filepicker_url || "http://placehold.it/800x500"
  end

  def contact_details
    [mobile, landline].join(" ")
  end

  def pin_img
    case category.name
    when "Hospital" then "http://maps.google.com/mapfiles/kml/pal2/icon38.png"
    when "Banks" then "http://maps.google.com/mapfiles/kml/pal2/icon50.png"
    when "Police" then "http://maps.google.com/mapfiles/kml/pal2/icon24.png"
    when "Restaurants" then "http://maps.google.com/mapfiles/kml/pal2/icon32.png"
    when "Hotel" then "http://maps.google.com/mapfiles/kml/pal2/icon28.png"
    when "Gasoline" then "http://maps.google.com/mapfiles/kml/pal2/icon29.png"
    when "Services" then "http://maps.google.com/mapfiles/kml/pal2/icon6.png"
    when "Government" then "http://maps.google.com/mapfiles/kml/pal2/icon0.png"
    end
  end


end
