class Category < ActiveRecord::Base

  validates :name, presence: true

  has_many :establishments, dependent: :destroy

  scope :by_name, proc { |name|  where(name: name) unless name.blank? }

end
