class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  scope :by_admins, -> { where(type: "Admin") }
  scope :by_partners, -> { where(type: "Partner") }

  def full_name
    [first_name, last_name].join(" ")
  end

end
