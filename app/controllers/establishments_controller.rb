class EstablishmentsController < ApplicationController

  before_action :authenticate_partner!
  before_action :set_options, only: [:edit, :new]
  before_action :find_establishment, only: [:edit, :update]

  def index
    @establishments = current_partner.establishments
  end

  def new
    @establishment = current_partner.establishments.new
  end

  def create
    @establishment = current_partner.establishments.create(establishment_params)
    redirect_to establishments_path
  end

  def edit
  end

  def update
    @establishment.update(establishment_params)
    redirect_to establishments_path
  end

  def destroy
  end

private

  def find_establishment
    @establishment = Establishment.find(params[:id])
  end

  def set_options
    @options = OptionsPresenter.new
  end

  def establishment_params
    params.require(:establishment).permit(:name, :address, :city_id, :category_id, :landline, :mobile, :open_hours, :filepicker_url)
  end

end
