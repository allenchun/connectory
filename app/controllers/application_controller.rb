class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :check_user_status

private

  def check_user_status
    if current_partner
      if current_partner.status == 'pending'
        sign_out current_partner
      end
    end
  end


end
