class SessionsController < Devise::SessionsController

  layout "authentication"

  protected
  def after_sign_in_path_for(resource)
    case resource.type
    when "Admin" then  rails_admin_path
    when "Partner" then partner_root_path
    end
  end

end
