class Api::V1::DirectoriesController < ApiController

  def cities
    cities = City.by_name(params[:name])
    render json: cities, status: 200
  end

  def categories
    categories = Category.by_name(params[:name])
    render json: categories, status: 200
  end

  def establishments
    establishments = Establishment.search(params)
    render json: establishments, status: 200
  end


end
