class LandingController < ApplicationController

  before_action :set_options

  def index
    render layout: "start"
  end

  def filter
    @establishments = Establishment.search(params)
    @places = @establishments.coordinates
  end


 private

 def set_options
   @options = OptionsPresenter.new
 end

end
