class RegistrationsController < Devise::RegistrationsController

  layout "authentication"

  protected
  def after_sign_up_path_for(resource)
    case resource.type
    when "Admin" then  rails_admin_path
    when "Partner" then  partner_root_path
    end
  end

end
