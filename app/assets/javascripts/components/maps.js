var map, current, marker, results, places, infowindow, pins;

$(function() {
  Connectory.maps.initialize = function(locations) {
    window.markers = []
    window.places = locations
    setCurrentPosition();
    $('.pin-tag').click(function() {
      var key = $(this).attr('data-marker');
      map.setZoom(16);
      map.panTo(pins[key].position);
      // infowindow.open(map, pins[key]);
    });
  };

  function showPins() {
    infowindow = new google.maps.InfoWindow;
    pins = {};
    marker = void 0;
    i = void 0;
    i = 0;
    while (i < places.length) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(places[i][1], places[i][2]),
        icon: places[i][6],
        map: map
      });
      markers.push(marker)
      pins[places[i][5]] = marker;
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          var info = '<div><strong>' + places[i][0] + '</strong><br><p>' + places[i][3] + '<br/>' + places[i][4] + '</p>'
          infowindow.setContent(info);
          infowindow.open(map, marker);
        };
      })(marker, i));

      i++;
    }
  }

  function setCurrentPosition(){
    if(navigator.geolocation){
      navigator.geolocation.getCurrentPosition(function(position) {
        current = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var manila = new google.maps.LatLng(14.600892, 121.068581);
        current = manila;
        renderMap();
      });
    }
  }

  function renderMap() {
    map = new google.maps.Map(document.getElementById('map-canvas'), {
      center: current,
      zoom: 15,
      panControl: true,
      mapTypeControl: true,
      rotateControl: true,
      scaleControl: false,
      streetViewControl: true,
      zoomControl: true,
      mapTypeControl:  true,
      navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    showPins();
  }



});
