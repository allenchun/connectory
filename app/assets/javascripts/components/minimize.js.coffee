$ ->

  Connectory.minimize.initialize = ->
    minimized_elements = $('p.minimize')
    minimized_elements.each ->
      t = $(this).text()
      if t.length < 50
        return
      $(this).html t.slice(0, 15) + '<span>... </span><a href="#" class="more">More...</a>' + '<span style="display:none;">' + t.slice(15, t.length) + ' <a href="#" class="less">Less</a></span>'
      return
    $('a.more', minimized_elements).click (event) ->
      event.preventDefault()
      $(this).hide().prev().hide()
      $(this).next().show()
      return
    $('a.less', minimized_elements).click (event) ->
      event.preventDefault()
      $(this).parent().hide().prev().show().prev().show()
      return
